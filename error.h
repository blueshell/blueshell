#ifndef __ERROR_H
#define __ERROR_H

// Exit codes used when encountering a serious error
#define ERR_EXIT_FORK -1
#define ERR_EXIT_CHNGHOME -2

void print_error(char* message);
void exit_with_error(int err_num);

#endif