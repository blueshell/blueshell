#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include "utils.h"

/* Exits the shell with the given error code. Calls the cleanup function
 * to ensure the shell is exited cleanly */
void exit_with_error(int err_num)
{
    cleanup();
    exit(err_num);
}

/* Prints an error message after a failed syscall. A user readable message is 
 * printed and supplemented by the standard syscall error message (given by
 * perror()) */
void print_error(char* message)
{
	printf("Error: %s: ", message);
	perror(NULL);
}