#define _XOPEN_SOURCE 700 // Use POSIX.1-2008 for compatibility with POSIX and C99

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include "error.h"
#include "builtins.h"
#include "utils.h"
#include "alias.h"
#include "easteregg.h"
#include "history.h"

/* Launches an external process. The binary name/path is the 0th entry of
 * input_tokens, the arguments are in the rest of the array. Forks, exec's the
 * child process then waits on it exiting. We assume input_tokens is a valid
 * string array to pass to execvp(), no checking is done on it at this stage */
void launch_external(char** input_tokens)
{
    int child_status;
    pid_t pid = fork();     

    if (pid < 0)
    {
        print_error("Error calling fork(), exiting");
        exit_with_error(ERR_EXIT_FORK);
    } 

    if (pid == 0) 
    {
        int result = execvp(input_tokens[0], input_tokens);

        if (result < 0)
            perror(NULL);

        exit(result);
    } 
    else 
    {   // Parent process
        wait(&child_status);
    }
}

/* Splits the input buffer ('buf') at space and tab characters and places
 * them into the 'raw_input_tokens' array. The final entry of the array is
 * always set to NULL */
void tokenize_input(char** raw_input_tokens, char* buf)
{
    char* tok;
    
    // Load up all the tokens
    raw_input_tokens[0] = strtok(buf, " \t");
    int i = 1;

    while ((tok = strtok(NULL, " \t")) != NULL && i < 50)
    {
        raw_input_tokens[i++] = tok;
    }

    raw_input_tokens[i] = NULL;
}

/* Constructs the final tokens array for a user input string given the input
 * tokens. Gets all alias tokens for the given command (raw_...[0]), adds them
 * to input_tokens, then adds all other raw_input_tokens to input_tokens. The
 * final entry of input_tokens will be a NULL */
int construct_tokens_array(char** input_tokens, char** raw_input_tokens)
{
    // Get the tokens for the alias if any.
    char* alias_tokens[50];
    int alias_result = get_alias_value(alias_tokens, raw_input_tokens[0]);

    char* tok;
    int i = 0;
    int j = 0;

    // Create tokens array based on alias tokens and user input tokens
    if (alias_result == 0)
    {
        j = 1;
        while ((tok = alias_tokens[i]) != NULL && i < 50)
            input_tokens[i++] = tok;
    }

    while((tok = raw_input_tokens[j++]) != NULL && i < 50)
        input_tokens[i++] = tok;

    if (raw_input_tokens[j-1] != NULL)
    {
        printf("Too many tokens, cannot execute command.\n");
        return -1;
    }

    input_tokens[i] = NULL;
    return 0;
}

/* Processes a line of user input to the shell. Returns 1 if the input given
 * should cause the shell to terminate execution, 0 if not */
int process_input_line(char* buf, int user_input)
{
    char* raw_input_tokens[50];
    char* input_tokens[50];

    int len;
    if ((len = strlen(buf)) > 1) 
    {
        if (user_input == 1)
            buf[len-1] = '\0';

        char* buf_copy = strdup(buf);
        tokenize_input(raw_input_tokens, buf);
        int result = construct_tokens_array(input_tokens, raw_input_tokens);
        
        if (result < 0)
            return 0;

        if (user_input == 1 && buf[0] != '!')
            add_history_entry(buf_copy);

        free(buf_copy);
        
        // Begin decoding
        if (strcmp(input_tokens[0], "exit") == 0)
        {
            return 1;
        }
        else if (strcmp(input_tokens[0], "cd") == 0)
        {
            cd(input_tokens);
        }
        else if (strcmp(input_tokens[0], "pwd") == 0)
        {
            pwd(input_tokens);
        }
        else if (strcmp(input_tokens[0], "getpath") == 0)
        {
            get_path(input_tokens);
        }
        else if (strcmp(input_tokens[0], "setpath") == 0)
        {
            set_path(input_tokens);
        }
        else if (strcmp(input_tokens[0], "alias") == 0)
        {
            alias(input_tokens);
        }
        else if (strcmp(input_tokens[0], "unalias") == 0)
        {
            unalias(input_tokens);
        }
        else if (strcmp(input_tokens[0], "history") == 0)
        {
            history(input_tokens);
        }
        else if (input_tokens[0][0] == '!') 
        {
            const char* result = nth_command(input_tokens);
            if(result != NULL)
            {
                return process_input_line(strcpy(buf, result), 0);
            }
            else
            {
                printf("History entry not found.\n");
            }
        }
        else if(strcmp(input_tokens[0], "easter") == 0)
        {
           print_koopa(); 
        }
        else if (strcmp(input_tokens[0], "") == 0)
        {
            // Do nothing for blank input
        }
        else 
        {
            launch_external(input_tokens);
        }
    }
    
    return 0;
}

/* Shell entry point. Initialises the shell, contains the main loop, then
 * deinitialises when execution is terminating */
int main(int argc, char** argv)
{
    int terminate = 0;
    char buf[512];

    const char* prompt = ">";
    orig_path = getenv("PATH");

    init_aliases();
    init_history();
    chdir_home();

    printf("%s", prompt);

    // In condition, get 512 bytes from stdin and ensure valid result
    while (!terminate && fgets(buf, 512, stdin) != NULL)
    {
        terminate = process_input_line(buf, 1);

        if (!terminate)
            printf("%s", prompt);
    }

    cleanup();
    return 0;
}
