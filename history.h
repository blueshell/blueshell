#ifndef __HISTORY_H
#define __HISTORY_H

#define MAX_HIST_SIZE 20
#define HISTORY_FILE_NAME ".hist_list"

#define HISTORY_SAVE_ERROR -1
#define HISTORY_READ_FAILED -2

typedef struct {
	int id;
	char* command;
} hist_entry_t;

void init_history();
void deinit_history();
void add_history_entry(char* line);
const char* get_history_entry(int n);
const char* get_last_history_entry();
void print_history();
int save_history();
int load_history();
FILE* get_history_file(char* mode);

#endif
