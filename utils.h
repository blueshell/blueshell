#ifndef __UTILS_H
#define __UTILS_H

char* orig_path; // Stores the system path as it was when the shell was started

void cleanup();
char* strjoin(char** elems, char* delim, int init_idx);
void strsplit(char** tokens, char* str, char* delim);
FILE* get_home_dir_file(char* mode, char* file_name);

#endif
