#define _XOPEN_SOURCE 700 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include "utils.h"
#include "alias.h"
#include "history.h"

/* Called when the shell is exiting, restores the path to it's original state
 * as it was when the shell was started, cleans up alias and history systems */
void cleanup()
{
	save_aliases();
	deinit_aliases();

	save_history();
	deinit_history();

    int result = setenv("PATH", orig_path, 1);

    if (result == -1)
    {
        perror(NULL);
    }
}

/* Joins the strings given in 'elems' into a single string, separated by 'delim',
 * starting with the string in the array position 'init_idx'. Returns the joined
 * string. The resulting string is dynamically allocated so should be freed when
 * no longer being used */
char* strjoin(char** elems, char* delim, int init_idx)
{
	int total_len = 0;
	int i = init_idx;
	while (elems[i] != NULL)
	{
		total_len += strlen(elems[i]) + strlen(delim);
		i++;
	}

	char* strbuf = calloc(total_len + 1, 1);

	int j = init_idx;
	while (elems[j] != NULL)
	{
		strcat(strbuf, elems[j]);
		strcat(strbuf, delim);
		j++;
	}

	return strbuf;
}

/* Splits the given string ('str' parameter) at each instance of the given 
 * delimiter. The resulting substrings are stored in the 'tokens' array. */
void strsplit(char** tokens, char* str, char* delim)
{
	tokens[0] = strtok(str, " \t");

	int i = 1;
    char* tok;

    while ((tok = strtok(NULL, " \t")) != NULL && i < 50)
    {
        tokens[i++] = tok;
    }
    tokens[i] = NULL;
}

/**
 * Convenience function to get a file from the users
 * home directory. Abstracts common code from history
 * and alias.
 */
FILE* get_home_dir_file(char* mode, char* file_name){
    FILE* file;
    char* home_dir = getenv("HOME");
    char* file_path;

    /* +2 on the alloc size:
       1 byte for the null char
       1 byte for the '/' to join the paths.
    */
    file_path = calloc(strlen(home_dir) + strlen(file_name) + 2, 1);

    if(file_path == NULL)
        return NULL;
    //calloc failed.

    strcat(file_path, home_dir);
    strcat(file_path, "/");
    strcat(file_path, file_name);

    //Do not attempt to open in read mode if the file does not exist.
    if((strcmp(mode, "r") == 0) && (access(file_path, F_OK) == -1))
    {
        free(file_path);
        return NULL;
    }

    file = fopen(file_path, mode);
    free(file_path);

    if(file == NULL)
    {
        perror(NULL);
    }

    return file;
}
