#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "alias.h"
#include "error.h"
#include "utils.h"

static alias_t aliases[MAX_ALIASES];

/* Initializes the array of alias_t structs */
void init_aliases()
{
	for (int i = 0; i < MAX_ALIASES; i++)
		aliases[i].active = 0;
    load_aliases();
}

/* Frees any dynamically allocated memory associated with alias
 * structures. Must be called before exiting the shell */
void deinit_aliases()
{
	for (int i = 0; i < MAX_ALIASES; i++)
	{
		if (aliases[i].alias != NULL)			
			free(aliases[i].alias);

		if (aliases[i].value != NULL)
			free(aliases[i].value);
	}
}

/* Checks if an alias of a given name already exists.
 * Returns 1 if it exists, 0 if not */
int alias_exists(char* alias)
{
	for (int i = 0; i < MAX_ALIASES; i++)
	{
		if (aliases[i].active == ALIAS_INACTIVE)
			continue;

		if (strcmp(aliases[i].alias, alias) == 0)
			return 1;
	}

	return 0;
}

/* Overwrites a pre-existing alias with a new value. The alias given
 * is assumed to already exist and be in a valid state. Returns 
 * ALIAS_OVERWRITTEN when successful, ADD_ALIAS_FAIL when unsuccessful
 * or ALIAS_MEM_ERR when memory allocation fails */
int alias_overwrite(char* alias, char* value)
{
	for (int i = 0; i < MAX_ALIASES; i++)
	{
		if (aliases[i].active == ALIAS_INACTIVE)
			continue;

		if (strcmp(aliases[i].alias, alias) == 0)
		{
			free(aliases[i].value);
			aliases[i].value = strdup(value);

			if (aliases[i].value == NULL)
			{
				free(aliases[i].alias);
				aliases[i].alias = NULL;
				return ALIAS_MEM_ERR;
			}

			return ALIAS_OVERWRITTEN;
		}
	}

	return ADD_ALIAS_FAIL;
}

/* Adds an alias. The string passed as 'alias' will be replaced with the string
 * passed as 'value' when being processed as a shell command. Returns 0 if the
 * alias was successfully added, returns ADD_ALIAS_FAIL if there was no space
 * to add the alias. Pre-existing aliases are overwritten, returning the value
 * ALIASES_OVERWRITTEN */
int add_alias(char* alias, char* value)
{
	// If alias already exists, overwrite it
	if (alias_exists(alias))
		return alias_overwrite(alias, value);

	for (int i = 0; i < MAX_ALIASES; i++)
	{
		if (aliases[i].active == ALIAS_INACTIVE)
		{
			aliases[i].active = ALIAS_ACTIVE;
			aliases[i].alias = strdup(alias);

			if (aliases[i].alias == NULL)
				return ALIAS_MEM_ERR;

			aliases[i].value = strdup(value);

			if (aliases[i].value == NULL)
			{
				// free() .alias to leave the struct in a valid state
				free(aliases[i].alias);
				aliases[i].alias = NULL;

				return ALIAS_MEM_ERR;
			}

			return 0;
		}
	}

	return ADD_ALIAS_FAIL;
}

/* Removes an alias. If an alias is registered under the name passed as 'alias',
 * it will be removed (the replace will no longer occur). Returns 0 if the 
 * alias was successfully removed, returns ALIAS_NOT_FOUND if it could not
 * find the alias whose removal was requested */
int remove_alias(char* alias)
{
	for (int i = 0; i < MAX_ALIASES; i++)
	{
		if (aliases[i].active == ALIAS_INACTIVE)
			continue;
		
		if (strcmp(aliases[i].alias, alias) == 0)
		{
			aliases[i].active = ALIAS_INACTIVE;

			free(aliases[i].alias);
			free(aliases[i].value);
			
			aliases[i].alias = NULL;
			aliases[i].value = NULL;

			return 0;
		}
	}

	return ALIAS_NOT_FOUND;
}

/* Prints all active aliases to stdout */
void print_out_aliases()
{
	for (int i = 0; i < MAX_ALIASES; i++)
	{
		if (aliases[i].active == ALIAS_ACTIVE)
		{
			printf("%s\t=>\t%s\n", aliases[i].alias, aliases[i].value);
		}
	}
}

/* Fills the given array of strings with the value of the given alias (if the
 * alias exists). Returns 0 if alias exists and tokens array is filled, 1 if
 * alias does not exist */
int get_alias_value(char** tokens, char* alias)
{
	if (alias_exists(alias) == 1)
	{
		for (int i = 0; i < MAX_ALIASES; i++)
		{
			if (aliases[i].active == ALIAS_ACTIVE)
			{
				if (strcmp(aliases[i].alias, alias) == 0)
				{
					char* value = strdup(aliases[i].value);
                    strsplit(tokens, value, " ");
					return 0;
				}
			}
		}
	}

	return 1;
}

/* Gets the file handle to the persistent alias file. Returns a pointer to the file
 * or NULL if an error was encountered */
FILE* get_alias_file(char* mode)
{
    return get_home_dir_file(mode, ALIAS_FILE_NAME);
}

/* Saves aliases to the persistent aliases file. Returns 0 if the save was
 * successful, returns ALIAS_SAVE_FAILED if the saving failed */
int save_aliases()
{
    FILE *alias_file = get_alias_file("w");

    if (alias_file == NULL)
    {
    	print_error("Could not write to aliases file");
        return ALIAS_SAVE_FAILED;
    }

    int result;

    for(int i = 0; i < MAX_ALIASES; i++)
    {
        if (aliases[i].active == ALIAS_ACTIVE)
        {
            result = fprintf(alias_file, "%s %s\n", aliases[i].alias, aliases[i].value);
            if(result == -1)
                return ALIAS_SAVE_FAILED;
        }
    }

    fclose(alias_file);
    return 0;
}

/* Loads aliases from the persistent aliases file into the aliases array.
 * Returns 0 on success, ALIAS_READ_FAILED if an error is encountered */
int load_aliases()
{
    FILE *alias_file = get_alias_file("r");
    char buf[512];
    char* tokens[50];
    if(alias_file == NULL)
    {
        return ALIAS_READ_FAILED;
    }

    while(fgets(buf, 512, alias_file) != NULL && strlen(buf) > 1)
    {
        buf[strlen(buf)-1] = '\0';
        strsplit(tokens, buf, " ");

        char* value = strjoin(tokens, " ", 1);
        add_alias(tokens[0], value);
        free(value);
    }
    fclose(alias_file);
    return 0;
}










