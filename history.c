#define _XOPEN_SOURCE 700 

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "history.h"
#include "error.h"
#include "utils.h"

static int count;
static int back;
static hist_entry_t entries[MAX_HIST_SIZE];

/**
 * Initialises variables then gets any stored history.
 */
void init_history()
{
	count = 0;
	back = 0;

	load_history();
}

/**
 * Frees up any allocated memory in preperation for exit.
 */
void deinit_history()
{
	for (int i = 0; i < MAX_HIST_SIZE; i++)
	{
		if (entries[i].command != NULL)
			free(entries[i].command);
	}
}

/**
 * Adds the command line to history with strdup allocated memory
 * Frees the memory currently used for the slot if it's being overwritten.
 */
void add_history_entry(char* line)
{
    /* Don't add oversized lines to history, they won't be executed so don't
	 * need to be saved in history */
	if (strlen(line) >= 512)
		return;

	int index = count % MAX_HIST_SIZE;

	entries[index].id = count + 1;

	if (entries[index].command != NULL)
		free(entries[index].command);

	entries[index].command = strdup(line);

	if (count > MAX_HIST_SIZE)
		back++;

	count++;
}

/**
 * Returns a pointer to the String for the last command.
 * NULL otherwise.
 */
const char* get_history_entry(int n) 
{
    for (int i = 0; i < MAX_HIST_SIZE; i++)
    	if (entries[i].id == n)
    		return entries[i].command;

	return NULL;
}

/**
 * Returns a pointer to the String for the last command.
 */
const char* get_last_history_entry()
{
    int index = (count - 1) % MAX_HIST_SIZE;
    return entries[index].command;
}

/**
 * Iterates over the circular-array and prints each history
 * line along with it's command.
 */
void print_history()
{
    int a = 0;
    while((entries[a].id != (count- MAX_HIST_SIZE + 1)) && (entries[a].id != 1)){
        a++;
    }

    int i = 0;
	while (i < MAX_HIST_SIZE && i < count)
	{
		printf("%d\t-\t%s\n", entries[a].id, entries[a].command);
		i++;
        a = (a + 1)%MAX_HIST_SIZE;
	}
}

/**
 * Convenience function to get the hist_list file.
 */
FILE* get_history_file(char* mode)
{
    return get_home_dir_file(mode, HISTORY_FILE_NAME); 
}

/**
 * Write the history array to the hist_list file.
 */
int save_history()
{
	FILE* hist_file = get_history_file("w");

	if (hist_file == NULL)
	{
		print_error("Could not save history file");
		return HISTORY_SAVE_ERROR;
	}

	// Print counter to first line
	fprintf(hist_file, "%d\n", count);

	for (int i = 0; i < MAX_HIST_SIZE; i++)
	{
		if (entries[i].command != NULL)
			fprintf(hist_file, "%d %s\n", entries[i].id, entries[i].command);
	}

	fclose(hist_file);

	return 0;
}

/**
 * Initialise the history array from the hist_list file.
 */
int load_history()
{
	FILE* hist_file = get_history_file("r");

	if (hist_file == NULL)
		return HISTORY_READ_FAILED;

	char buf[512];
	char* tokens[50];

	if (fgets(buf, 512, hist_file) != NULL)
	{
		    int n;
		    n = atoi(buf);

		    if (n == 0)
		    	return HISTORY_READ_FAILED;
		    
		    count = n;
	}
	else
		return HISTORY_READ_FAILED;

	int i = 0;
	while (fgets(buf, 512, hist_file) != NULL && strlen(buf) > 1)
	{
		buf[strlen(buf)-1] = '\0';
        strsplit(tokens, buf, " ");
        char* value = strjoin(tokens, " ", 1);
        
        int x = atoi(tokens[0]);

        if (x == 0)
        	return HISTORY_READ_FAILED;

        entries[i].id = x;

        if (entries[i].command != NULL)
        	free(entries[i].command);

        entries[i].command = strdup(value);

        if (entries[i].command == NULL)
        	return HISTORY_READ_FAILED;

        i++;
        free(value);
	}

	fclose(hist_file);
	return 0;
}
