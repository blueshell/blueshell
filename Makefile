CC=gcc
CFLAGS = -Wall -pedantic -std=c99 -c
C_SOURCES = shell.c error.c builtins.c utils.c alias.c easteregg.c history.c
C_OBJECTS = $(C_SOURCES:.c=.o)

all: shell

clean:
	rm -f *.o
	rm -f shell
	rm -f ~/.hist_list
	rm -f ~/.aliases

shell: $(C_OBJECTS)
	$(CC) $(C_OBJECTS) -o shell

.c.o:
	$(CC) $(CFLAGS) $< -o $@
