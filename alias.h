#ifndef __ALIAS_H
#define __ALIAS_H

#define MAX_ALIASES 10

// Alias error/status codes
#define ADD_ALIAS_FAIL -1
#define ALIAS_NOT_FOUND -2
#define ALIAS_MEM_ERR -3
#define ALIAS_SAVE_FAILED -4
#define ALIAS_READ_FAILED -5
#define ALIAS_OVERWRITTEN 1

// Alias struct (in)activity values
#define ALIAS_INACTIVE 0
#define ALIAS_ACTIVE 1

// File name(s)
#define ALIAS_FILE_NAME ".aliases"
typedef struct {
	unsigned int active;
	char *alias;
	char *value;
} alias_t;

void init_aliases();
void deinit_aliases();
int add_alias(char* alias, char* value);
int alias_overwrite(char* alias, char* value);
int remove_alias(char* alias);
void print_out_aliases();
int get_alias_value(char** tokens, char* alias);
char* alias_replace(char* arg);
int save_aliases();
int load_aliases();
FILE* get_alias_file(char* mode);

#endif
