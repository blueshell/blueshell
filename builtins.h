#ifndef __BUILTINS_H
#define __BUILTINS_H

void chdir_home();
void cd(char** args);
void pwd(char** args);
void get_path(char** args);
void set_path(char** args);
void history(char** args);
const char* last_command(char** args);
const char* nth_command(char** args);
void alias(char** args);
void print_aliases(char** args);
void set_alias(char** args);
void unalias(char** args);

#endif
