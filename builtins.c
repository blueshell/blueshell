#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "builtins.h"
#include "error.h"
#include "alias.h"
#include "utils.h"
#include "history.h"

/* Changes the current directory to the user's home directory. If an error
 * is encountered when changing directories, the shell exits */
void chdir_home()
{
    int chdir_result = chdir(getenv("HOME"));

    if (chdir_result == -1)
    {
        print_error("Could not change to home directory");
        exit_with_error(ERR_EXIT_CHNGHOME);
    }
}

/* Change directory (cd) built in command. If no args are given, we change
 * to the user's home directory. The tilde character is taken to represent
 * the path to the user's homedir (as in bash etc.) */
void cd(char** args)
{
	if (args[1] == NULL)
	{
		chdir_home();
	}
	else if (args[2] == NULL)
	{
		char* path = strdup(args[1]);
		char* orig_cwd = getcwd(NULL, 1024);
		int result;

		if (path[0] == '~') 
		{
			chdir_home();

            // Check that the argument isn't "~/"
			if (path[1] == '/' && path[2] != '\0')
			{
				int arglen = strlen(path) - 2;

				char* subpath = malloc(arglen + 1);

				if (subpath == NULL)
				{
					print_error("Memory allocation failed during tilde expansion");
					return;
				}

				strcpy(subpath, &path[2]);
				result = chdir(subpath);
				free(subpath);
			}
		}
		else 
		{
			result = chdir(path);
		}

		if (result == -1)
		{
			perror(NULL);

			/* It's possible after a failed tilde expand cd to be in the home 
			   directory instead of the cwd when the cd was called, attempt
			   to return to the original directory if not currently there */
			if (strcmp(getcwd(NULL, 1024), orig_cwd) != 0)
			{
				int restore_result = chdir(orig_cwd);

				if (restore_result == -1)
					print_error("Could not restore original path after cd fail");
			}
		}

		free(orig_cwd);
		free(path);
	}
	else 
	{
		printf("Usage: cd [path]\n");
	}
}

/* Print working directory (pwd) built in command. Prints an error if the 
 * current working directory path cannot be loaded */
void pwd(char** args)
{
    char* path = getcwd(NULL, 1024);

    if (path == NULL)
    {
        perror(NULL);
        return;
    }

    printf("%s\n", path);
    free(path);
}

/* Prints the current system path ('getpath' builtin) */
void get_path(char** args)
{
	printf("%s\n", getenv("PATH"));
}

/* Sets the shell's path. Prints an error if the path couldn't be set */
void set_path(char** args)
{
	// Parameter checking [*, param, NULL, ...]
	if (args[1] != NULL && args[2] == NULL)
	{
		int result = setenv("PATH", args[1], 1);

		if (result == -1)
		{
			print_error("Could not set path");
		}
	}
	else
	{
		printf("Usage: setpath <pathlist>\n");
	}
}

/* Prints the current shell aliases */
void print_aliases(char** args)
{
    print_out_aliases();
}

/* Sets an alias, passed in the args array. The args[1] is taken as the 
 * alias key, the rest of the args are joined into a string and taken as
 * the alias replacement value */
void set_alias(char** args)
{
	char* strbuf = strjoin(args, " ", 2);
    int result = add_alias(args[1], strbuf);
    free(strbuf);

    if (result == ADD_ALIAS_FAIL)
    {
        printf("Maximum number of aliases exceded - did not add alias.\n");
    }
    else if (result == ALIAS_MEM_ERR)
    {
    	printf("Memory allocation failed when adding alias - did not add alias.\n");
    }
    else if (result == ALIAS_OVERWRITTEN)
    {
    	printf("The alias '%s' has been over-written with the new value\n", args[1]);
    }
}

/* The 'alias' built in command. If no args given, the alias list is printed,
 * otherwise a new alias is set */ 
void alias(char** args)
{
    if (args[1] == NULL)
   		print_out_aliases();	
   	else if (args[1] != NULL && args[2] != NULL)
   		set_alias(args);
   	else
   		printf("Usage: alias [<alias> <value>]\n");
}

/* The 'unalias' built in command. Removes an alias whose key is given in
 * args[1] */
void unalias(char** args){
    if(args[2] == NULL)
    {
        int result = remove_alias(args[1]);
        if(result == ALIAS_NOT_FOUND)
        {
            printf("Alias does not exist.\n");
        }
    }
    else
    {
        printf("Usage: unalias <alias>\n");
    }
}

/* The 'history' built in command. Prints the shell history */
void history(char** args)
{
	if(args[1] == NULL)
    {
        print_history();
    }
    else
    {
        printf("Usage: history\n");
    }
}

/* Gets the most recent command typed into the shell */
const char* last_command(char** args)
{
    if((args[1] == NULL) && strlen(args[0]) == 2)
    {
    	return get_last_history_entry();
    }
    else
    {
        printf("Usage: !!\n");
        return NULL;
    }
}

/* Gets the n'th command from history. Implementation for '!!' and '!n' built
 * in commands. Returns the string of the requested command */
const char* nth_command(char** args)
{
    if(args[1] == NULL)
    {
        if(args[0][1] == '!')
        {
            return last_command(args);
        }
        else
        {
            int n;
            char* argcopy = strdup(args[0]);
            n = atoi(argcopy+1);
            free(argcopy);

            if(n == 0)
            {
                return NULL;
            }
            else
            {
            	return get_history_entry(n);
            }
        }
    }
    else
    {
        printf("Usage: ![!|no.]\n");
        return NULL;
    }

}
